import { createStore, applyMiddleware, compose } from 'redux'
import reducers from '../reducers'
import { routerMiddleware } from 'react-router-redux'
import { hashHistory } from 'react-router'
//import createSagaMiddleware from 'redux-saga'
//import sagas from '../sagas/index'

export default function configureStore(initialState) {
  const enhacer = compose(
    applyMiddleware(
      routerMiddleware(hashHistory)
    ),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
  const store = createStore(reducers, initialState, enhacer)

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers')
      store.replaceReducer(nextReducer)
    })
  }

  return store
}
