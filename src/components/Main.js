require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import WeUI from 'react-weui'
import 'weui'

const { Button, Dialog } = WeUI
const { Alert, Confirm } = Dialog

let yeomanImage = require('../images/yeoman.png');

class AppComponent extends React.Component {

  state = {
    showAlert: false,
    showConfirm: false,
    alert:{
      title: '标题标题',
      buttons: [
        {
        label: '好的',
        onClick: this.hideAlert.bind(this)
}
      ]
    },
    confirm: {
      title: '标题标题',
      buttons: [
        {
          type: 'default',
          label: '好的',
          onClick: this.hideConfirm.bind(this)
        },
        {
          type: 'primary',
          label: '我愿意',
          onClick: this.hideConfirm.bind(this)
        }
      ]
    }
  };
  showAlert() {
    this.setState({showAlert:true})
  }
  hideAlert() {
    this.setState({showAlert:false})
  }
  showConfirm() {
    this.setState({showConfirm:true})
  }
  hideConfirm() {
    this.setState({showConfirm:false})
  }
  render() {
    return (
      <div className="index">
        <img src={yeomanImage} alt="Yeoman Generator" />
        <div className="notice">OA</div>
        <Button>Hello wechat</Button>
        <Button type="warn" onClick={this.showAlert.bind(this)}>警告你</Button>
        <Button type="primary" onClick={this.showConfirm.bind(this)}>确认</Button>
        <Alert
          show={this.state.showAlert}
          title={this.state.alert.title}
          buttons={this.state.alert.buttons}
        >
          <h1>你好！！！</h1>
        </Alert>
        <Confirm
          show={this.state.showConfirm}
          title={this.state.confirm.title}
          buttons={this.state.confirm.buttons}
        >
          <h1>确定吗？</h1>
        </Confirm>
      </div>
    );
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;
