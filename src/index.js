import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Router, Route, hashHistory} from 'react-router'
import {addTodo, toggleTodo, setVisibilityFilter, VisibilityFilters} from 'actions/const'
//import { syncReduxAndRouter } from 'react-router-redux'
import configureStore from './stores'
import App from './containers/App'

const store = configureStore();

let unsubscribe = store.subscribe(() => console.log(store.getState()))
store.dispatch(addTodo('learn action'))
store.dispatch(addTodo('learn reducer'))
store.dispatch(addTodo('learn store'))
store.dispatch(toggleTodo(0))
store.dispatch(toggleTodo(1))
store.dispatch(setVisibilityFilter(VisibilityFilters.SHOW_COMPLETED))
unsubscribe();

render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById('app')
);
