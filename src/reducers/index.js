/* Combine all available reducers to a single root reducer.
 *
 * CAUTION: When using the generators, this file is modified in some places.
 *          This is done via AST traversal - Some of your formatting may be lost
 *          in the process - no functionality should be broken though.
 *          This modifications only run once when the generator is invoked - if
 *          you edit them, they are not updated again.
 */
import { combineReducers } from 'redux';
import { VisibilityFilters, SET_VIBILITY_FILTER, ADD_TODO, TOGGLE_TODO} from 'actions/const'
const { SHOW_ALL } = VisibilityFilters
/* Populated by react-webpack-redux:reducer */

function todos (state = [], action) {
  switch (action.type) {
    case ADD_TODO:
      return [
        ...state,
        {
          text: action.text,
          completed: false
        }
      ]
    case TOGGLE_TODO:
      return state.map((todo, index) => {
        if( index === action.index){
          return Object.assign({}, todo, {
            completed: !todo.completed
          })
        }
        return todo
      })
    default:
      return state
  }
}

function visibilityFilter (state = SHOW_ALL, action) {
  switch (action.type) {
    case SET_VIBILITY_FILTER:
        return action.filter
    default:
        return state;
  }
}

const reducers = {
  visibilityFilter,
  todos
};
module.exports = combineReducers(reducers);
